/**
 * Define controls format and networking protocol.
 * Controls byte packet format:
 *      [0000SSFF]
 * F - forward state, 2 bits
 * S - steering state, 2 bits
 * 0 - unused
 */

using System;
using ProtoBuf;

namespace XRowboat {

    public enum RowboatControlSteeringState: uint {
        None = 0,
        Right = 1,
        Left = 2,
    }


    public enum RowboatControlForwardState: uint {
        None = 0,
        Forward = 1,
        Reverse = 2,
    }

    public enum RowboatSeat: uint {
        Front = 0,
        Rear = 1,
    }


    /**
     * Packet from client -> server with client rowboat controls state
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientToServerRowboatControls {
        public byte Payload;
    }

    /**
     * Packet from client -> server requesting mounting a seat of BoatEntityId
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientToServerMountRowboat {
        public long BoatEntityId;
        public byte Seat;
    }

    /**
     * Packet from server -> client player indicating their mount attempt
     * of BoatEntityId was successful
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketServerToClientMountRowboatSuccess {
        public long BoatEntityId;
        public byte Payload; // [RequireOar | AllowRearControls | AllowFrontControls | Seat]
    

        public static PacketServerToClientMountRowboatSuccess FromEntitySeatAndSettings(
            long boatEntityId,
            RowboatSeat seat,
            bool allowFrontSeatControl,
            bool allowRearSeatControl,
            bool requireOar
        ) {
            return new PacketServerToClientMountRowboatSuccess() {
                BoatEntityId = boatEntityId,
                Payload = (byte) ((uint) seat | ( Convert.ToUInt32(allowFrontSeatControl) << 2) | ( Convert.ToUInt32(allowRearSeatControl) << 3) | ( Convert.ToUInt32(requireOar) << 4))
            };
        }
    }

    // Convenience struct to unpack the packet fields from payload
    public struct PacketServerToClientMountRowboatSuccessUnpacked {
        public long EntityId;
        public RowboatSeat Seat;
        public bool AllowFrontSeatControl;
        public bool AllowRearSeatControl;
        public bool RequireOar;

        public static PacketServerToClientMountRowboatSuccessUnpacked FromPacket(PacketServerToClientMountRowboatSuccess packet) {
            return new PacketServerToClientMountRowboatSuccessUnpacked() {
                EntityId = packet.BoatEntityId,
                Seat = (RowboatSeat) ((uint) packet.Payload & 0b0011),
                AllowFrontSeatControl = (packet.Payload & 0b0100) != 0,
                AllowRearSeatControl = (packet.Payload & 0b1000) != 0,
                RequireOar = (packet.Payload & 0b10000) != 0,
            };
        }
    }
    
    /**
     * Packet from client -> server notifying player
     * unmounted a seat of BoatEntityId.
     */
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class PacketClientToServerUnmountRowboat {
        public long BoatEntityId;
        public byte Seat;
    }

    // Packet from Server -> Client that an entity successfully mounted a boat entity.
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct PacketServerToClientEntityMountedRowboat {
        public long BoatEntityId;
        public long PassengerEntityId;
        public byte Seat;

        public static PacketServerToClientEntityMountedRowboat FromEntityAndSeat(long boatEntityId, long passengerEntityId, RowboatSeat seat) {
            return new PacketServerToClientEntityMountedRowboat() {
                BoatEntityId = boatEntityId,
                PassengerEntityId = passengerEntityId,
                Seat = (byte) (seat),
            };
        }
    }

    // Packet from Server -> Client that an entity successfully unmounted a boat entity.
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct PacketServerToClientEntityUnmountedRowboat {
        public long BoatEntityId;
        public long PassengerEntityId;
        public byte Seat;

        public static PacketServerToClientEntityUnmountedRowboat FromEntityAndSeat(long boatEntityId, long passengerEntityId, RowboatSeat seat) {
            return new PacketServerToClientEntityUnmountedRowboat() {
                BoatEntityId = boatEntityId,
                PassengerEntityId = passengerEntityId,
                Seat = (byte) (seat),
            };
        }
    }


    // TODO: is this necessary?
    // Packet from Server -> Client to periodically sync
    // rowboat passengers
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public struct PacketServerToClientRowboatSync {
        public long BoatEntityId;
        public long PassengerFrontEntityId;
        public long PassengerRearEntityId;
        public bool HasPassengerFront;
        public bool HasPassengerRear;

        public static PacketServerToClientRowboatSync FromEntityAndSeat(
            long boatEntityId,
            long passengerFrontEntityId,
            long passengerRearEntityId,
            bool hasPassengerFront,
            bool hasPassengerRear
        ) {
            return new PacketServerToClientRowboatSync() {
                BoatEntityId = boatEntityId,
                PassengerFrontEntityId = passengerFrontEntityId,
                PassengerRearEntityId = passengerRearEntityId,
                HasPassengerFront = hasPassengerFront,
                HasPassengerRear = hasPassengerRear,
            };
        }
    }

    public readonly struct RowboatControls {
        public RowboatSeat Seat { get; }
        public RowboatControlForwardState Forward { get; }
        public RowboatControlSteeringState Steering { get; }
        

        public RowboatControls(RowboatSeat seat, RowboatControlForwardState forward, RowboatControlSteeringState steering) {
            Seat = seat;
            Forward = forward;
            Steering = steering;
        }

        public RowboatControls(uint seat, uint forward, uint steering) {
            Seat = (RowboatSeat) seat;
            Forward = (RowboatControlForwardState) forward;
            Steering = (RowboatControlSteeringState) steering;
        }
        
        public override string ToString() {
            return String.Format("RowboatControls {{ seat={0}, forward={1}, steering={2} }}", this.Seat, this.Forward, this.Steering);
        }

        public static RowboatControls FromPacket(byte payload) {
            uint state = (uint) payload;
            uint seat = state & 0b0011;
            uint forward = (state & 0b1100) >> 2;
            uint steering = (state & 0b110000) >> 4;
            return new RowboatControls(seat, forward, steering);
        }

        public PacketClientToServerRowboatControls ToPacket() {
            uint payload = (uint) this.Seat | ((uint) this.Forward << 2) | ((uint) this.Steering << 4);
            
            return new PacketClientToServerRowboatControls() {
                Payload = (byte) payload,
            };
        }
    }

}