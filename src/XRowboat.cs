﻿using System;
using Vintagestory.API;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;
using Vintagestory.GameContent;

[assembly: ModInfo( "xrowboat",
    Description = "Rowboat",
    Website     = "",
    Authors     = new []{ "xeth" } )]

namespace XRowboat
{
    public class XRowboatMod : ModSystem
    {
        public static string PATH { get; } = "xrowboat";

        public static string CONFIG_PATH { get; } = "xrowboat_config.json";

        public static XRowboatConfig Config = new XRowboatConfig();

        // internal variable names
        internal static string ATTRIBUTE_ROWING = "xrowboatRowing";
        internal static string ATTRIBUTE_PASSENGER_FRONT = "hasPassengerFront";
        internal static string ATTRIBUTE_PASSENGER_FRONT_ID = "passengerFront";
        internal static string ATTRIBUTE_PASSENGER_REAR = "hasPassengerRear";
        internal static string ATTRIBUTE_PASSENGER_REAR_ID = "passengerRear";

        // api ref
        internal static ICoreServerAPI sapi;
        internal static ICoreClientAPI capi;

        // packet channels
        internal static IClientNetworkChannel clientChannel;
        internal static IServerNetworkChannel serverChannel;


        public override void Start(ICoreAPI api) {
            api.RegisterEntity(EntityRowboat.NAME, typeof(EntityRowboat));

            api.RegisterItemClass(ItemRowboat.NAME, typeof(ItemRowboat));
            api.RegisterItemClass(ItemOar.NAME, typeof(ItemOar));

            api.RegisterMountable(EntityRowboatSeat.NAME, EntityRowboatSeat.GetMountable);
            
            // load config
            try {
                XRowboatConfig loadedConfig = api.LoadModConfig<XRowboatConfig>(XRowboatMod.CONFIG_PATH);
                if ( loadedConfig != null ) {
                    XRowboatMod.Config = loadedConfig;
                }
                else {
                    api.StoreModConfig(XRowboatMod.Config, CONFIG_PATH);
                }
            }
            catch ( System.Exception e ) {
                System.Console.WriteLine("{0} Failed to load mod config.", e);
            }
        }

        public override void StartClientSide(ICoreClientAPI api) {
            capi = api;

            clientChannel =
                api.Network.RegisterChannel("xrowboat")
                .RegisterMessageType(typeof(PacketClientToServerRowboatControls))
                .RegisterMessageType(typeof(PacketClientToServerMountRowboat))
                .RegisterMessageType(typeof(PacketServerToClientMountRowboatSuccess))
                .RegisterMessageType(typeof(PacketClientToServerUnmountRowboat))
                .RegisterMessageType(typeof(PacketServerToClientEntityMountedRowboat))
                .RegisterMessageType(typeof(PacketServerToClientEntityUnmountedRowboat))
                .SetMessageHandler<PacketServerToClientMountRowboatSuccess>(ClientOnMountSuccessPacket)
                .SetMessageHandler<PacketServerToClientEntityMountedRowboat>(ClientOnEntityMountedPacket)
                .SetMessageHandler<PacketServerToClientEntityUnmountedRowboat>(ClientOnEntityUnmountedPacket)
            ;
        }

        public override void StartServerSide(ICoreServerAPI api) {
            sapi = api;

            serverChannel =
                api.Network.RegisterChannel("xrowboat")
                .RegisterMessageType(typeof(PacketClientToServerRowboatControls))
                .RegisterMessageType(typeof(PacketClientToServerMountRowboat))
                .RegisterMessageType(typeof(PacketServerToClientMountRowboatSuccess))
                .RegisterMessageType(typeof(PacketClientToServerUnmountRowboat))
                .RegisterMessageType(typeof(PacketServerToClientEntityMountedRowboat))
                .RegisterMessageType(typeof(PacketServerToClientEntityUnmountedRowboat))
                .SetMessageHandler<PacketClientToServerRowboatControls>(ServerOnClientControlPacket)
                .SetMessageHandler<PacketClientToServerMountRowboat>(ServerOnMountPacket)
                .SetMessageHandler<PacketClientToServerUnmountRowboat>(ServerOnUnmountPacket)
            ;
        }

        public static void ClientSendMountPacket(long boatEntityId, RowboatSeat seat) {
            XRowboatMod.clientChannel.SendPacket(new PacketClientToServerMountRowboat() {
                BoatEntityId = boatEntityId,
                Seat = (byte) seat,
            });
        }

        public static void ClientSendUnmountPacket(long boatEntityId, RowboatSeat seat) {
            XRowboatMod.clientChannel.SendPacket(new PacketClientToServerUnmountRowboat() {
                BoatEntityId = boatEntityId,
                Seat = (byte) seat,
            });
        }

        public static void ClientSendControlPacket(RowboatControls controls) {
            XRowboatMod.clientChannel.SendPacket(controls.ToPacket());
        }

        private void ServerOnClientControlPacket(IPlayer fromPlayer, PacketClientToServerRowboatControls packet) {
            EntityPlayer playerEntity = fromPlayer.Entity;
            if ( playerEntity == null ) {
                return;
            }

            IMountable mount = playerEntity.MountedOn;
            if ( mount == null ) {
                return;
            }

            if ( mount is EntityRowboatSeat ) {
                EntityRowboatSeat seat = (EntityRowboatSeat) mount;
                EntityRowboat boat = seat.Host;
                boat.SetControls(RowboatControls.FromPacket(packet.Payload));
            }
        }
        
        private void ServerOnMountPacket(IPlayer fromPlayer, PacketClientToServerMountRowboat packet) {
            Entity boatEntitySelected = sapi.World.GetEntityById(packet.BoatEntityId);
            RowboatSeat seat = (RowboatSeat) packet.Seat;

            if ( boatEntitySelected != null && boatEntitySelected is EntityRowboat ) {
                EntityRowboat boat = (EntityRowboat) boatEntitySelected;
                bool successfulMount = false;

                if ( seat == RowboatSeat.Front ) {
                    if ( boat.SeatFront.Passenger == null || boat.SeatFront.Passenger?.EntityId == fromPlayer.Entity.EntityId ) {
                        successfulMount = fromPlayer.Entity.TryMount(boat.SeatFront);
                    }
                }
                else { // rear
                    if ( boat.SeatRear.Passenger == null || boat.SeatRear.Passenger?.EntityId == fromPlayer.Entity.EntityId ) {
                        successfulMount = fromPlayer.Entity.TryMount(boat.SeatRear);
                    }
                }

                if ( successfulMount ) {
                    // send mount success to player, sends seats + controls config
                    serverChannel.SendPacket(PacketServerToClientMountRowboatSuccess.FromEntitySeatAndSettings(
                        packet.BoatEntityId,
                        seat,
                        XRowboatMod.Config.AllowFrontSeatToControlBoat,
                        XRowboatMod.Config.AllowRearSeatToControlBoat,
                        XRowboatMod.Config.RequireOar
                    ),
                    fromPlayer as IServerPlayer);

                    // // send mount success packet to other players
                    // serverChannel.BroadcastPacket(PacketServerToClientEntityMountedRowboat.FromEntityAndSeat(
                    //     packet.BoatEntityId,
                    //     fromPlayer.Entity.EntityId,
                    //     seat
                    // ),
                    // fromPlayer as IServerPlayer); // except fromPlayer
                }
            }
        }

        private void ServerOnUnmountPacket(IPlayer fromPlayer, PacketClientToServerUnmountRowboat packet) {
            Entity boatEntity = sapi.World.GetEntityById(packet.BoatEntityId);
            RowboatSeat seat = (RowboatSeat) packet.Seat;

            if ( boatEntity != null && boatEntity is EntityRowboat ) {
                EntityRowboat boat = (EntityRowboat) boatEntity;
                bool successfulUnmount = false;

                if ( seat == RowboatSeat.Front ) {
                    if ( boat.SeatFront.Passenger != null || boat.SeatFront.Passenger?.EntityId == fromPlayer.Entity.EntityId ) {
                        successfulUnmount = fromPlayer.Entity.TryUnmount();
                    }
                }
                else { // rear
                    if ( boat.SeatRear.Passenger != null || boat.SeatRear.Passenger?.EntityId == fromPlayer.Entity.EntityId ) {
                        successfulUnmount = fromPlayer.Entity.TryUnmount();
                    }
                }

                if ( successfulUnmount ) {
                    // // send unmount packet to other players?
                    // serverChannel.BroadcastPacket(PacketServerToClientEntityUnmountedRowboat.FromEntityAndSeat(
                    //     packet.BoatEntityId,
                    //     fromPlayer.Entity.EntityId,
                    //     seat
                    // ),
                    // fromPlayer as IServerPlayer); // except fromPlayer
                }
            }
        }

        private void ClientOnMountSuccessPacket(PacketServerToClientMountRowboatSuccess packet) {
            Entity boatEntitySelected = capi.World.GetEntityById(packet.BoatEntityId);

            if ( boatEntitySelected != null && boatEntitySelected is EntityRowboat ) {
                EntityRowboat boat = (EntityRowboat) boatEntitySelected;
                IPlayer player = capi.World.Player;

                // unpack packet
                PacketServerToClientMountRowboatSuccessUnpacked unpacked = PacketServerToClientMountRowboatSuccessUnpacked.FromPacket(packet);
                RowboatSeat seat = unpacked.Seat;
                bool allowFrontSeatControls = unpacked.AllowFrontSeatControl;
                bool allowRearSeatControls = unpacked.AllowRearSeatControl;
                bool requireOar = unpacked.RequireOar;

                // set boat flags, theres probably better way to sync server settings...
                boat.ClientAllowFrontSeatControl = allowFrontSeatControls;
                boat.ClientAllowRearSeatControl = allowRearSeatControls;
                boat.ClientRequireOar = requireOar;

                bool successfulMount; // for debugging
                if ( seat == RowboatSeat.Front ) {
                    successfulMount = player.Entity.TryMount(boat.SeatFront);
                }
                else { // rear
                    successfulMount = player.Entity.TryMount(boat.SeatRear);
                }
                
                // try to stop swimming/flying animation, does not work
                player.Entity.StopAnimation("swim");
                player.Entity.StopAnimation("swimidle");
                player.Entity.StopAnimation("glide");
            }
        }

        private void ClientOnEntityMountedPacket(PacketServerToClientEntityMountedRowboat packet) {
            Entity boatEntity = capi.World.GetEntityById(packet.BoatEntityId);
            Entity passengerEntity = capi.World.GetEntityById(packet.PassengerEntityId);
            RowboatSeat seat = (RowboatSeat) packet.Seat;

            if ( boatEntity != null &&
                passengerEntity != null &&
                boatEntity is EntityRowboat &&
                passengerEntity is EntityAgent &&
                packet.PassengerEntityId != capi.World.Player.Entity.EntityId
            ) {
                EntityRowboat boat = (EntityRowboat) boatEntity;
                EntityAgent passenger = (EntityAgent) passengerEntity;

                if ( seat == RowboatSeat.Front ) {
                    passenger.TryMount(boat.SeatFront);
                }
                else { // rear
                    passenger.TryMount(boat.SeatRear);
                }
            }
        }

        private void ClientOnEntityUnmountedPacket(PacketServerToClientEntityUnmountedRowboat packet) {
            Entity boatEntity = capi.World.GetEntityById(packet.BoatEntityId);
            Entity passengerEntity = capi.World.GetEntityById(packet.PassengerEntityId);
            RowboatSeat seat = (RowboatSeat) packet.Seat;

            if ( boatEntity != null &&
                passengerEntity != null &&
                boatEntity is EntityRowboat &&
                passengerEntity is EntityAgent &&
                packet.PassengerEntityId != capi.World.Player.Entity.EntityId
            ) {
                EntityAgent passenger = (EntityAgent) passengerEntity;
                passenger.TryUnmount();
            }
        }

    }

    /**
     * Json mod config 
     */
    public class XRowboatConfig
    {
        // ============================================================
        // high level mechanics
        // ============================================================
        // require `oar` item in hand to control rowboat
        public bool RequireOar = true;

        // allow front seat to control boat
        public bool AllowFrontSeatToControlBoat = true;

        // allow rear seat player to also control boat
        // disable => minecraft style boat (only front controls)
        public bool AllowRearSeatToControlBoat = true;

        // ============================================================
        // rowboat speed/control mechanics
        // ============================================================
        // max forward/reverse speed in water
        // for single player and double player rowing
        public double MaxForwardSpeedSingle = 0.15;
        public double MaxReverseSpeedSingle = -0.05;
        public double MaxForwardSpeedDouble = 0.18;
        public double MaxReverseSpeedDouble = -0.1;

        // max forward/reverse speed on land (no dual player setting)
        public double MaxForwardSpeedOnLand = 0.02;
        public double MaxReverseSpeedOnLand = -0.02;

        // forward/reverse acceleration per player (sign should be positive)
        public double ForwardAcceleration = 0.005;
        public double ReverseAcceleration = 0.005;

        // multiplicative water drag deceleration
        public double DragDecelerationFactor = 0.8;
        
        // clamp value before setting speed to 0
        public double ClampSpeedToZeroThreshold = 0.1;

        // turning acceleration per player
        public double TurnAcceleration = 0.002;

        // multiplicative water drag deceleration
        public double DragTurningFactor = 0.6;

        // max rotation rates
        public double MaxAngularVelocitySingle = 0.05;
        public double MaxAngularVelocityDouble = 0.08;

        // ============================================================
        // plugin detailed mechanics
        // ============================================================
        // animation to play when using not using oar
        public string OarIdleAnimation = "holdinglanternrighthand";
        // animation to play when using oar while moving rowboat
        public string OarRowForwardAnimation = "row_forward";
        public string OarRowReverseAnimation = "row_reverse";

        // max block distance for mounting
        public double MaxMountDistance = 2.5;

        // TODO: how many times ticks per sending current controls packet to server
        public int PacketSendPeriod = 2;

        // TODO: how many ticks to wait before starting deceleration
        // must be long enough to wait for next controls packets to come
        public int TicksBeforeDeceleration = 4;

        public int ClientDebugPeriod = 20;
    }
}
