using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;

namespace XRowboat
{
    // Seat player mount positioning is simple, only allow
    // vertical offset and offset along boat forward axis:
    //  _____             | - - - rear offset distance
    //  |   |     |       |
    //  |   |     |      0,0
    //  |   |     |       |
    //   \_/      v       | - - - front offset distance
    //         forward
    // 
    // In this simplified model, player can only be offset
    // along the boat's forward axis.
    public class EntityRowboatSeat : IMountable
    {
        public static string NAME = "EntityRowboatSeat";

        public readonly EntityRowboat Host;

        public readonly RowboatSeat Seat;

        public readonly float MountOffsetY; // vertical player offset

        public readonly float MountOffsetDist; // offset scalar along boat forward axis

        public EntityControls controls = new EntityControls();

        public EntityAgent Passenger = null;

        public EntityRowboatSeat() {
            // 1.16 ??
            controls.OnAction = this.onControls;
        }
        
        public static IMountable GetMountable(IWorldAccessor world, TreeAttribute tree)
        {
            Entity entityHost = world.GetEntityById(tree.GetLong("hostId"));
            if ( entityHost != null && entityHost is EntityRowboat ) {
                RowboatSeat seat = (RowboatSeat) tree.GetInt("seat");
                if ( seat == RowboatSeat.Front ) {
                    return (entityHost as EntityRowboat).SeatFront;
                }
                else if ( seat == RowboatSeat.Rear ) {
                    return (entityHost as EntityRowboat).SeatRear;
                }
            }
            return null;
        }

        public EntityRowboatSeat(EntityRowboat host, RowboatSeat rowboatSeat, float mountOffsetDist, float mountOffsetY)
        {
            this.Host = host;
            this.Seat = rowboatSeat;
            this.MountOffsetY = mountOffsetY;
            this.MountOffsetDist = mountOffsetDist;
        }

        public Vec3d MountPosition
        {
            get {
                // equivalent to:
                // return this.Host.SidedPos.XYZ.AheadCopy(MountOffsetDist, 0f, this.Host.SidedPos.Yaw).OffsetCopy(0.0, MountOffsetY, 0.0);
                return this.Host.SidedPos.XYZ.AddCopy(
                    MountOffsetDist * -Math.Cos(this.Host.SidedPos.Yaw),
                    MountOffsetY,
                    MountOffsetDist * Math.Sin(this.Host.SidedPos.Yaw)
                );
            }
        }

        public string SuggestedAnimation
        {
            get { return "sitflooridle"; }
        }

        public EntityControls Controls
        {
            get { return this.controls; }
        }

        public float? MountYaw
        {
            get { return this.Host.SidedPos.Yaw; }
        }
        
        public void DidUnmount(EntityAgent entityAgent)
        {
            // Console.WriteLine("[{0}] DidUnmount: {1}", this.Host.World.Side, entityAgent);

            this.Passenger = null;

            // client side debugging
            // if ( this.Host.World.Side == EnumAppSide.Client ) {
            //     XRowboatMod.capi.World.Player.ShowChatNotification(String.Format("[DidUnmount] host={0}, entity={1}, entity.MountedOn={2}",
            //         this.Host,
            //         entityAgent,
            //         entityAgent.MountedOn
            //     ));
            // }
        }

        public void DidMount(EntityAgent entityAgent)
        {
            // Console.WriteLine("[{0}] DidMount: {1}", this.Host.World.Side, entityAgent);

            if ( this.Passenger != null && this.Passenger != entityAgent ) {
                // Console.WriteLine("[{0}] DidMount: {1} INVALID, PASSENGER != NULL && PASSENGER != ENTITY", this.Host.World.Side, entityAgent);
                this.Passenger.TryUnmount();
                return;
            }

            this.Passenger = entityAgent;

            // server side: set attribute used to sync mounted entities
            // across different clients
            if ( this.Host.World.Side == EnumAppSide.Server ) {
                if ( this.Seat == RowboatSeat.Front ) {
                    this.Host.WatchedAttributes.SetBool(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT, true);
                    this.Host.WatchedAttributes.SetLong(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT_ID, entityAgent.EntityId);
                }
                else {
                    this.Host.WatchedAttributes.SetBool(XRowboatMod.ATTRIBUTE_PASSENGER_REAR, true);
                    this.Host.WatchedAttributes.SetLong(XRowboatMod.ATTRIBUTE_PASSENGER_REAR_ID, entityAgent.EntityId);
                }
            }

            // client side debugging
            // if ( this.Host.World.Side == EnumAppSide.Client ) {
            //     XRowboatMod.capi.World.Player.ShowChatNotification(String.Format("[DidMount] host={0}, entity={1}, entity.MountedOn={2}",
            //         this.Host,
            //         entityAgent,
            //         entityAgent.MountedOn
            //     ));
            // }

            // try stop certain common animations, does not work
            entityAgent.StopAnimation("swim");
            entityAgent.StopAnimation("swimidle");
            entityAgent.StopAnimation("glide");
        }

        public void MountableToTreeAttributes(TreeAttribute tree)
        {
            tree.SetString("className", EntityRowboatSeat.NAME);
            tree.SetLong("hostId", this.Host.EntityId);
            tree.SetInt("seat", (int) this.Seat);
        }

        internal void onControls(EnumEntityAction action, bool on, ref EnumHandling handled)
        {
            // Console.WriteLine("[{0}] onControls", this.Host.World.Side);

            // unmounting issue: when gently tapping sneak (shift) to unmount
            // sometimes this does not send to server, so player unmounts on
            // client but not server.
            // so instead: unmount on client, send unmount packet to server
            // to unmount the corresponding seat
            if ( this.Host.World.Side == EnumAppSide.Client ) {
                if ( action == EnumEntityAction.Sneak && on ) {
                    this.Passenger?.TryUnmount();
                    controls.StopAllMovement();
                    XRowboatMod.ClientSendUnmountPacket(this.Host.EntityId, this.Seat);
                }
            }
        }
    }


    /**
     * Intermediate movement control state for each controller seat
     */
    internal struct RowboatControlMotionState
    {
        public double NewForwardSpeed;
        public double NewAngularVelocity;
        public int NumAccelerating; // number players accelerating
        public int NumTurning; // number players turning

        public RowboatControlMotionState(double newForwardSpeed, double newAngularVelocity, int numAccelerating, int numTurning)
        {
            this.NewForwardSpeed = newForwardSpeed;
            this.NewAngularVelocity = newAngularVelocity;
            this.NumAccelerating = numAccelerating;
            this.NumTurning = numTurning;
        }
    }


    public class EntityRowboat : Entity
    {
        public static string NAME { get; } = "EntityRowboat";        

        // TODO: should be configurable attributes in entity
        public float MountOffsetY = 0.27f;
        public float MountOffsetDistFront = -0.8f;
        public float MountOffsetDistRear = 0.5f;

        public EntityRowboatSeat SeatFront;
        public EntityRowboatSeat SeatRear;

        public RowboatControls ControlsFront = new RowboatControls(RowboatSeat.Front, RowboatControlForwardState.None, RowboatControlSteeringState.None);
        public RowboatControls ControlsRear = new RowboatControls(RowboatSeat.Rear, RowboatControlForwardState.None, RowboatControlSteeringState.None);
        
        // client-side flags, will be changed when player mounts
        // theres probably better way to sync server side settings...
        internal bool ClientAllowFrontSeatControl = true;
        internal bool ClientAllowRearSeatControl = true;
        internal bool ClientRequireOar = true;

        // current forward speed
        public double ForwardSpeed = 0.0;

        // current turning speed (rad/tick)
        public double AngularVelocity = 0.0;

        // for debugging
        private int ClientDebugTick = 0;

        public override bool ApplyGravity
        {
            get { return true; }
        }

        public override bool IsInteractable
        {
            get { return true; }
        }


        public override float MaterialDensity
        {
            get { return 100f; }
        }


        public override double SwimmingOffsetY
        {
            get { return 0.45; }
        }


        public override void Initialize(EntityProperties properties, ICoreAPI api, long InChunkIndex3d)
        {
            base.Initialize(properties, api, InChunkIndex3d);
            
            this.SeatFront = new EntityRowboatSeat(this, RowboatSeat.Front, this.MountOffsetDistFront, this.MountOffsetY);
            this.SeatRear = new EntityRowboatSeat(this, RowboatSeat.Rear, this.MountOffsetDistRear, this.MountOffsetY);
            
            // need custom controls handler for dismounting
            this.SeatFront.controls.OnAction = this.SeatFront.onControls;
            this.SeatRear.controls.OnAction = this.SeatRear.onControls;

            if (api.Side == EnumAppSide.Server) {
                // GetBehavior<EntityBehaviorPassivePhysics>().OnPhysicsTickCallback = onPhysicsTickCallback;
            }
        }

        private void onPhysicsTickCallback(float dtFac)
        {
            
        }


        public override void OnGameTick(float dt)
        {
            base.OnGameTick(dt);

            // Console.WriteLine("[SERVER] front={0}, rear={1}", this.SeatFront.Passenger, this.SeatRear.Passenger);

            // periodically check if seats are occupied + cleanup if not
            if ( this.SeatFront.Passenger != null && (!this.SeatFront.Passenger.Alive || this.SeatFront.Passenger.MountedOn != this.SeatFront) ) {
                this.SeatFront.Passenger = null;
            }
            if ( this.SeatRear.Passenger != null && (!this.SeatRear.Passenger.Alive || this.SeatRear.Passenger.MountedOn != this.SeatRear) ) {
                this.SeatRear.Passenger = null;
            }

            // Console.WriteLine("Swimming: {0}", this.Swimming);
            // Console.WriteLine("FeetInLiquid: {0}", this.FeetInLiquid);
            // Console.WriteLine("Forward front {0}", this.SeatFront.controls.Forward);
            // Console.WriteLine("Forward rear {0}", this.SeatRear.controls.Forward);

            // ================================================================
            // SERVER SIDE
            // ================================================================
            if ( this.World.Side == EnumAppSide.Server ) {
                // handles syncing player unmount (front)
                if ( this.SeatFront.Passenger == null && this.WatchedAttributes.GetBool(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT, false) ) {
                    this.WatchedAttributes.SetBool(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT, false);
                    // send that previous passenger unmounted to other players
                    long passengerId = this.WatchedAttributes.GetLong(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT_ID);
                    XRowboatMod.serverChannel.BroadcastPacket(PacketServerToClientEntityUnmountedRowboat.FromEntityAndSeat(
                        this.EntityId,
                        passengerId,
                        RowboatSeat.Front
                    ));
                }
                // handles syncing player unmount (rear)
                if ( this.SeatRear.Passenger == null && this.WatchedAttributes.GetBool(XRowboatMod.ATTRIBUTE_PASSENGER_REAR, false) ) {
                    this.WatchedAttributes.SetBool(XRowboatMod.ATTRIBUTE_PASSENGER_REAR, false);
                    // send that previous passenger unmounted to other players
                    long passengerId = this.WatchedAttributes.GetLong(XRowboatMod.ATTRIBUTE_PASSENGER_REAR_ID);
                    XRowboatMod.serverChannel.BroadcastPacket(PacketServerToClientEntityUnmountedRowboat.FromEntityAndSeat(
                        this.EntityId,
                        passengerId,
                        RowboatSeat.Rear
                    ));
                }

                // current motion state
                RowboatControlMotionState controlState = new RowboatControlMotionState(
                    this.ForwardSpeed,
                    this.AngularVelocity,
                    0,
                    0
                );

                EntityAgent seatFrontPassenger = this.SeatFront.Passenger;
                EntityAgent seatRearPassenger = this.SeatRear.Passenger;

                // front seat controls
                if ( seatFrontPassenger != null && XRowboatMod.Config.AllowFrontSeatToControlBoat ) {
                    controlState = handleServerPlayerControls(seatFrontPassenger, this.ControlsFront, controlState);
                }

                // rear seat controls
                if ( seatRearPassenger != null && XRowboatMod.Config.AllowRearSeatToControlBoat  ) {
                    controlState = handleServerPlayerControls(seatRearPassenger, this.ControlsRear, controlState);
                }

                // update forward motion
                if ( controlState.NumAccelerating == 2 ) { // both players moving forward
                    if ( this.FeetInLiquid ) { // in water
                        this.ForwardSpeed = MathUtils.Clamp(controlState.NewForwardSpeed, XRowboatMod.Config.MaxReverseSpeedDouble, XRowboatMod.Config.MaxForwardSpeedDouble);
                    } else { // on land
                        this.ForwardSpeed = MathUtils.Clamp(controlState.NewForwardSpeed, XRowboatMod.Config.MaxReverseSpeedOnLand, XRowboatMod.Config.MaxForwardSpeedOnLand);
                    }
                }
                else if ( controlState.NumAccelerating == 1 ) { // only 1 player accelerating forward
                    if ( this.FeetInLiquid ) { // in water
                        if ( this.ForwardSpeed < XRowboatMod.Config.MaxReverseSpeedSingle ) { // if 2nd player stops rowing, decelerate to max single player speed
                            this.ForwardSpeed += XRowboatMod.Config.ForwardAcceleration;
                        }
                        if ( this.ForwardSpeed > XRowboatMod.Config.MaxForwardSpeedSingle ) { // if 2nd player stops rowing, decelerate to max single player speed
                            this.ForwardSpeed -= XRowboatMod.Config.ForwardAcceleration;
                        }
                        else {
                            this.ForwardSpeed = MathUtils.Clamp(controlState.NewForwardSpeed, XRowboatMod.Config.MaxReverseSpeedSingle, XRowboatMod.Config.MaxForwardSpeedSingle);
                        }
                    } else { // on land
                        this.ForwardSpeed = MathUtils.Clamp(controlState.NewForwardSpeed, XRowboatMod.Config.MaxReverseSpeedOnLand, XRowboatMod.Config.MaxForwardSpeedOnLand);
                    }
                }
                else { // drag when no players accelerating
                    if ( this.ForwardSpeed > XRowboatMod.Config.ClampSpeedToZeroThreshold || this.ForwardSpeed < -XRowboatMod.Config.ClampSpeedToZeroThreshold ) {
                        this.ForwardSpeed *= XRowboatMod.Config.DragDecelerationFactor;
                    }
                    else if ( this.ForwardSpeed != 0.0 ) {
                        this.ForwardSpeed = 0.0;
                        this.SidedPos.Motion.Set(0f, 0f, 0f);
                    }
                }

                // update turning motion
                if ( controlState.NumTurning == 2 ) { // both players turning
                    this.AngularVelocity = MathUtils.Clamp(controlState.NewAngularVelocity, -XRowboatMod.Config.MaxAngularVelocityDouble, XRowboatMod.Config.MaxAngularVelocityDouble);
                }
                else if ( controlState.NumTurning == 1 ) {
                    if ( this.AngularVelocity < -XRowboatMod.Config.MaxAngularVelocitySingle ) {
                        this.AngularVelocity += XRowboatMod.Config.TurnAcceleration;
                    }
                    else if ( this.AngularVelocity > XRowboatMod.Config.MaxAngularVelocitySingle ) {
                        this.AngularVelocity -= XRowboatMod.Config.TurnAcceleration;
                    }
                    else {
                        this.AngularVelocity = MathUtils.Clamp(controlState.NewAngularVelocity, -XRowboatMod.Config.MaxAngularVelocitySingle, XRowboatMod.Config.MaxAngularVelocitySingle);
                    }
                }
                else { // drag when no players turning
                    if ( this.AngularVelocity > 0.01 || this.AngularVelocity < -0.01 ) {
                        this.AngularVelocity *= XRowboatMod.Config.DragTurningFactor;
                    }
                    else {
                        this.AngularVelocity = 0.0;
                    }
                }

                // finally update actual entity state
                if ( this.ForwardSpeed != 0.0 ) {
                    this.SidedPos.Motion = this.SidedPos.GetViewVector().Mul((float) -this.ForwardSpeed).ToVec3d();
                }
                if ( this.AngularVelocity != 0.0 ) {
                    const float pi2 = (float) (2.0 * Math.PI);
                    const float npi2 = (float) (-2.0 * Math.PI);

                    this.SidedPos.Yaw += (float) this.AngularVelocity;

                    // clamp within [-2pi, 2pi] otherwise angle interpolation gets fucked
                    // Note: this clamp will cause boat angle to visually "jump", issue
                    // with vintagestory angle interpolation? 
                    if ( this.SidedPos.Yaw > pi2 ) {
                        this.SidedPos.Yaw -= pi2;
                    }
                    else if ( this.SidedPos.Yaw < npi2 ) {
                        this.SidedPos.Yaw += pi2;
                    }
                }
            }
            // ================================================================
            // CLIENT SIDE
            // ================================================================
            else if ( this.World.Side == EnumAppSide.Client ) {
                // // makes sure both client side entities are synced
                // // DO WE EVEN NEED THIS? IDK LOL
                // // y would: in mineman, when entities loaded they may still have passengers
                // //          so additional client or periodic packet syncs needed
                // // but with watched attributes, can we have edge cases where
                // // client just received entity mounted/unmounted packets but watched attributes
                // // haven't updated?

                // // if ( this.WatchedAttributes.HasAttribute(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT) ) {
                //     bool hasFrontPassenger = this.WatchedAttributes.GetBool(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT, false);

                //     if ( this.SeatFront.Passenger == null && hasFrontPassenger ) {
                //         long entityId = this.WatchedAttributes.GetLong(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT_ID);
                //         if ( entityId != XRowboatMod.capi.World.Player.Entity.EntityId ) { // don't run twice for client player
                //             Entity entity = XRowboatMod.capi.World.GetEntityById(entityId);
                //             if ( entity != null && entity is EntityAgent ) {
                //                 (entity as EntityAgent).TryMount(this.SeatFront);
                //             }
                //         }
                //     }
                //     else if ( this.SeatFront.Passenger != null && !hasFrontPassenger ) {
                //         long entityId = this.WatchedAttributes.GetLong(XRowboatMod.ATTRIBUTE_PASSENGER_FRONT_ID);
                //         if ( entityId != XRowboatMod.capi.World.Player.Entity.EntityId ) { // don't run twice for client player
                //             Entity entity = XRowboatMod.capi.World.GetEntityById(entityId);
                //             if ( entity != null && entity is EntityAgent ) {
                //                 (entity as EntityAgent).TryUnmount();
                //             }
                //         }
                //     }
                // // }

                // // same shit for rear seat
                // // if ( this.WatchedAttributes.HasAttribute(XRowboatMod.ATTRIBUTE_PASSENGER_REAR) ) {
                //     bool hasRearPassenger = this.WatchedAttributes.GetBool(XRowboatMod.ATTRIBUTE_PASSENGER_REAR, false);

                //     if ( this.SeatRear.Passenger == null && hasRearPassenger ) {
                //         long entityId = this.WatchedAttributes.GetLong(XRowboatMod.ATTRIBUTE_PASSENGER_REAR_ID);
                //         if ( entityId != XRowboatMod.capi.World.Player.Entity.EntityId ) { // don't run twice for client player
                //             Entity entity = XRowboatMod.capi.World.GetEntityById(entityId);
                //             if ( entity != null && entity is EntityAgent ) {
                //                 bool status = (entity as EntityAgent).TryMount(this.SeatRear);
                //                 XRowboatMod.capi.World.Player.ShowChatNotification(String.Format("TRIED MOUNT {0}: {1}", entityId, status));
                //             }
                //         }
                //     }
                //     else if ( this.SeatRear.Passenger != null && !hasRearPassenger ) {
                //         long entityId = this.WatchedAttributes.GetLong(XRowboatMod.ATTRIBUTE_PASSENGER_REAR_ID);
                //         if ( entityId != XRowboatMod.capi.World.Player.Entity.EntityId ) { // don't run twice for client player
                //             Entity entity = XRowboatMod.capi.World.GetEntityById(entityId);
                //             if ( entity != null && entity is EntityAgent ) {
                //                 bool status = (entity as EntityAgent).TryUnmount();
                //                 XRowboatMod.capi.World.Player.ShowChatNotification(String.Format("TRIED UNMOUNT {0}: {1}", entityId, status));
                //             }
                //         }
                //     }
                // // }
                
                // this.ClientDebugTick += 1;
                // if ( this.ClientDebugTick >= XRowboatMod.Config.ClientDebugPeriod ) {
                //     XRowboatMod.capi.World.Player.ShowChatNotification(String.Format("front={0}, rear={1} (front.mountedOn={2}, rear.mountedOn={3})",
                //         this.SeatFront.Passenger,
                //         this.SeatRear.Passenger,
                //         this.SeatFront.Passenger?.MountedOn,
                //         this.SeatRear.Passenger?.MountedOn
                //     ));
                    
                //     this.ClientDebugTick = 0;
                // }

                // handle player controls
                if (  this.ClientAllowFrontSeatControl && this.SeatFront.Passenger == XRowboatMod.capi.World.Player.Entity ) {
                    handleClientRowboatControls(XRowboatMod.capi.World.Player.Entity, RowboatSeat.Front, this.SeatFront.Controls);
                }
                else if ( this.ClientAllowRearSeatControl && this.SeatRear.Passenger == XRowboatMod.capi.World.Player.Entity ) {
                    handleClientRowboatControls(XRowboatMod.capi.World.Player.Entity, RowboatSeat.Rear, this.SeatRear.Controls);
                }
            }
        }


        private RowboatControlMotionState handleServerPlayerControls(in EntityAgent passenger, in RowboatControls controls, RowboatControlMotionState motionState)
        {
            // check oar requirement
            if ( XRowboatMod.Config.RequireOar && (!(passenger.RightHandItemSlot.Itemstack?.Item is ItemOar) || !(passenger.LeftHandItemSlot.Itemstack?.Item is ItemOar))  ) {
                return motionState;
            }

            double newForwardSpeed = motionState.NewForwardSpeed;
            double newAngularVelocity = motionState.NewAngularVelocity;
            int numAccelerating = motionState.NumAccelerating;
            int numTurning = motionState.NumTurning;

            // default player motion variables for animations
            passenger.WatchedAttributes.SetInt(XRowboatMod.ATTRIBUTE_ROWING, 0);

            // turning movement
            if ( controls.Steering == RowboatControlSteeringState.Left ) {
                newAngularVelocity += XRowboatMod.Config.TurnAcceleration;
                numTurning += 1;

                // update player motion variables for animations
                passenger.WatchedAttributes.SetInt(XRowboatMod.ATTRIBUTE_ROWING, 1);
            }
            else if ( controls.Steering == RowboatControlSteeringState.Right ) {
                newAngularVelocity -= XRowboatMod.Config.TurnAcceleration;
                numTurning += 1;

                // update player motion variables for animations
                passenger.WatchedAttributes.SetInt(XRowboatMod.ATTRIBUTE_ROWING, -1);
            }

            // forward/reverse movement
            // make sure this is done after turning so forward/reverse has priority
            // in setting player animation attribute ATTRIBUTE_ROWING
            if ( controls.Forward == RowboatControlForwardState.Forward ) {
                newForwardSpeed += XRowboatMod.Config.ForwardAcceleration;
                numAccelerating += 1;

                // update player motion variables for animations
                passenger.WatchedAttributes.SetInt(XRowboatMod.ATTRIBUTE_ROWING, 1);
            }
            else if ( controls.Forward == RowboatControlForwardState.Reverse ) {
                newForwardSpeed -= XRowboatMod.Config.ForwardAcceleration;
                numAccelerating += 1;

                // update player motion variables for animations
                passenger.WatchedAttributes.SetInt(XRowboatMod.ATTRIBUTE_ROWING, -1);
            }

            return new RowboatControlMotionState(
                newForwardSpeed,
                newAngularVelocity,
                numAccelerating,
                numTurning
            );
        }


        private void handleClientRowboatControls(EntityAgent passenger, RowboatSeat seat, in EntityControls controls)
        {
            // check oar requirement
            if ( this.ClientRequireOar && (!(passenger.RightHandItemSlot.Itemstack?.Item is ItemOar) || !(passenger.LeftHandItemSlot.Itemstack?.Item is ItemOar)) ) {
                if ( controls.Forward || controls.Backward || controls.Left || controls.Right ) {
                    XRowboatMod.capi.TriggerIngameError(this, "xrowboat-error-no-oar", Lang.Get("xrowboat:error-no-oar"));
                }
                return;
            }

            uint forward;
            uint steering;

            if ( controls.Forward ) {
                forward = (uint) RowboatControlForwardState.Forward;
            }
            else if ( controls.Backward ) {
                forward = (uint) RowboatControlForwardState.Reverse;
            }
            else {
                forward = (uint) RowboatControlForwardState.None;
            }
            
            if ( controls.Left ) {
                steering = (uint) RowboatControlSteeringState.Left;
            }
            else if ( controls.Right ) {
                steering = (uint) RowboatControlSteeringState.Right;
            }
            else {
                steering = (uint) RowboatControlSteeringState.None;
            }

            XRowboatMod.ClientSendControlPacket(new RowboatControls((uint) seat, forward, steering));
        }


        public void SetControls(RowboatControls controls)
        {
            if ( controls.Seat == RowboatSeat.Front ) {
                this.ControlsFront = controls;
            }
            else {
                this.ControlsRear = controls;
            }
        }


        public override void OnInteract(EntityAgent byEntity, ItemSlot itemslot, Vec3d hitPosition, EnumInteractMode mode) {
            if ( mode != EnumInteractMode.Interact ) {
                return;
            }

            // sneak + click to remove boat
            if ( byEntity.Controls.Sneak && this.IsEmpty() ) {
                ItemStack stack = new ItemStack(byEntity.World.GetItem(this.Code));
                if ( !byEntity.TryGiveItemStack(stack) ) {
                    byEntity.World.SpawnItemEntity(stack, ServerPos.XYZ);
                }
                Die();
                return;
            }

            // client-only mount attempt + send request
            // do this on client because hitPosition determines mounting front or rear seat
            if ( byEntity.World.Side == EnumAppSide.Client ) {

                // distance check
                Double dist = byEntity.Pos.DistanceTo(this.Pos.XYZ);
                if ( dist > XRowboatMod.Config.MaxMountDistance ) {
                    return;
                }

                // check if player clicked front or rear side of boat
                Vec3d boatDirection = MathUtils.Vec3dFromYaw(this.SidedPos.Yaw);
                Vec3d hitDirection = hitPosition.Normalize();
                Double hitDotProd = hitDirection.X * boatDirection.X + hitDirection.Z * boatDirection.Z;
                if ( hitDotProd > 0.0 ) { // front (-90, 90) degrees
                    // byEntity.TryMount(this.SeatFront);
                    if ( byEntity.MountedOn != this.SeatFront ) {
                        XRowboatMod.ClientSendMountPacket(this.EntityId, RowboatSeat.Front);
                    }
                }
                else {
                    // byEntity.TryMount(this.SeatRear);
                    if ( byEntity.MountedOn != this.SeatRear ) {
                        XRowboatMod.ClientSendMountPacket(this.EntityId, RowboatSeat.Rear);
                    }
                }
            }
        }

        public override bool CanCollect(Entity byEntity)
        {
            return false;
        }

        public override void ToBytes(BinaryWriter writer, bool forClient)
        {
            base.ToBytes(writer, forClient);
        }

        public override void FromBytes(BinaryReader reader, bool fromServer)
        {
            base.FromBytes(reader, fromServer);
        }

        /**
         * Return if boat has Passengers
         */
        public bool IsEmpty() {
            return this.SeatFront.Passenger == null && this.SeatRear.Passenger == null;
        }
    }
}
